# Camelot Market

Common code for [Camelot](https://gitlab.com/camelot-project/camelot) and its
components.

## License

Distributed under the Eclipse Public License either version 1.0 or (at your option) any later version.
