(ns camelot.market.test-runner
  (:require [doo.runner :refer-macros [doo-tests]]
            [camelot.market.config-test]
            [camelot.market.impl.config-test]
            [camelot.market.impl.constants-test]
            [camelot.market.impl.environ-test]
            [camelot.market.impl.paths-test]
            [camelot.market.impl.platform-test]
            [camelot.market.impl.util-test]))

(doo-tests 'camelot.market.config-test
           'camelot.market.impl.config-test
           'camelot.market.impl.constants-test
           'camelot.market.impl.environ-test
           'camelot.market.impl.paths-test
           'camelot.market.impl.platform-test
           'camelot.market.impl.util-test)
