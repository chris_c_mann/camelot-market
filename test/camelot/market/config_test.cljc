(ns camelot.market.config-test
  (:require [camelot.market.config :as sut]
            [camelot.market.impl.paths :as paths]
            [camelot.market.impl.io :as io]
            [camelot.market.impl.config :as config]
            #?(:clj [clojure.test :refer [deftest testing is]]
               :cljs [cljs.test :refer [deftest testing is]])))

(deftest test-read-config
  (testing "read-config"
    (with-redefs [paths/initialise-paths identity
                  sut/write-config! (fn [c _] c)
                  config/read-config (constantly {:datasets {:default {:name "default"
                                                                       :paths {:database "/path/to/database"}}}})
                  paths/get-default-dataset-paths (constantly {:database "/path/to/database"})
                  paths/get-default-application-paths (constantly {:root "/path/to/root"})
                  paths/get-historic-paths (constantly {:database "/historic/path/to/database"})
                  paths/get-config-file (constantly "some/file")]

      (testing "should return the expected config"
        (is (= {:paths {:root "/path/to/root"}
                :server {:media-importers 4
                         :http-port 5341
                         :jvm-extra-args ""}
                :detector {:enabled false
                           :api-url "https://api.prod.camelotproject.org"
                           :confidence-threshold 0.9}
                :datasets {:default {:name "default"
                                     :paths {:database "/path/to/database"}}}
                :language :en
                :species-name-style :scientific
                :send-usage-data false
                :dev-mode false
                :open-browser-on-startup true
                :java-command "java"}
               (dissoc (sut/read-config) :client-id))))

      (testing "should initialise the expected paths"
        (let [calls (atom [])]
          (with-redefs [paths/initialise-paths #(do (swap! calls conj %)
                                                    %)]
            (sut/read-config)
            (is (= #{{:root "/path/to/root"} {:database "/path/to/database"}} (set @calls))))))

      (testing "should override default configuration with read-config"
        (with-redefs [config/read-config (constantly {:server {:media-importers 8}
                                                      :species-name-style :common})]
          (is (= {:paths {:root "/path/to/root"}
                  :server {:http-port 5341
                           :media-importers 8
                           :jvm-extra-args ""}
                  :detector {:enabled false
                             :api-url "https://api.prod.camelotproject.org"
                             :confidence-threshold 0.9}
                  :datasets {:default {:name "default"
                                       :paths {:database "/historic/path/to/database"}}}
                  :language :en
                  :species-name-style :common
                  :send-usage-data false
                  :dev-mode false
                  :open-browser-on-startup true
                  :java-command "java"}
                 (dissoc (sut/read-config) :client-id)))))

      (testing "should not force a default dataset"
        (with-redefs [config/read-config (constantly {:server {:media-importers 8}
                                                      :species-name-style :common
                                                      :datasets {:special {:paths {:database "/path/to/special/database"}}}})]
          (is (= {:paths {:root "/path/to/root"}
                  :server {:http-port 5341
                           :media-importers 8
                           :jvm-extra-args ""}
                  :detector {:enabled false
                             :api-url "https://api.prod.camelotproject.org"
                             :confidence-threshold 0.9}
                  :datasets {:special {:name "special"
                                       :paths {:database "/path/to/special/database"}}}
                  :language :en
                  :species-name-style :common
                  :send-usage-data false
                  :dev-mode false
                  :open-browser-on-startup true
                  :java-command "java"}
                 (dissoc (sut/read-config) :client-id)))))

      (testing "should add in new default paths which are not present in the configuration"
        (with-redefs [config/read-config (constantly {:server {:media-importers 8}
                                                      :paths {:root "/custom/path/to/root"}
                                                      :species-name-style :common})
                      paths/get-default-application-paths (constantly {:root "/path/to/root"
                                                                       :log "/path/to/logs"})]
          (is (= {:paths {:root "/custom/path/to/root"
                          :log "/path/to/logs"}
                  :server {:http-port 5341
                           :media-importers 8
                           :jvm-extra-args ""}
                  :detector {:enabled false
                             :api-url "https://api.prod.camelotproject.org"
                             :confidence-threshold 0.9}
                  :datasets {:default {:name "default"
                                       :paths {:database "/historic/path/to/database"}}}
                  :language :en
                  :species-name-style :common
                  :send-usage-data false
                  :dev-mode false
                  :open-browser-on-startup true
                  :java-command "java"}
                 (dissoc (sut/read-config) :client-id)))))

      (testing "should read the expected default configuration"
        (with-redefs [config/read-config (constantly nil)
                      paths/get-default-application-paths (constantly {:root "/path/to/root"
                                                                       :log "/path/to/logs"})]
          (is (= {:paths {:log "/path/to/logs"
                          :root "/path/to/root"}
                  :server {:http-port 5341
                           :media-importers 4
                           :jvm-extra-args ""}
                  :detector {:enabled false
                             :api-url "https://api.prod.camelotproject.org"
                             :confidence-threshold 0.9}
                  :datasets {:default {:name "default"
                                       :paths {:database "/path/to/database"}}}
                  :language :en
                  :species-name-style :scientific
                  :send-usage-data false
                  :dev-mode false
                  :open-browser-on-startup true
                  :java-command "java"}
                 (dissoc (sut/read-config) :client-id)))))

      (testing "should use the default backups location if not set in configuration"
        (with-redefs [config/read-config (constantly {:server {:media-importers 8}
                                                      :paths {:root "/custom/path/to/root"}
                                                      :datasets {:default {}}
                                                      :species-name-style :common})
                      paths/get-default-dataset-paths (constantly {:database "/path/to/database"
                                                                   :backup "/path/to/backups/default"})
                      paths/get-default-application-paths (constantly {:root "/path/to/root"
                                                                       :backup "/path/to/backups"
                                                                       :log "/path/to/logs"})]
          (is (= {:paths {:log "/path/to/logs"
                          :root "/custom/path/to/root"
                          :backup "/path/to/backups"}
                  :server {:http-port 5341
                           :media-importers 8
                           :jvm-extra-args ""}
                  :detector {:enabled false
                             :api-url "https://api.prod.camelotproject.org"
                             :confidence-threshold 0.9}
                  :datasets {:default {:name "default"
                                       :paths {:database "/path/to/database"
                                               :backup "/path/to/backups/default"}}}
                  :language :en
                  :species-name-style :common
                  :send-usage-data false
                  :dev-mode false
                  :open-browser-on-startup true
                  :java-command "java"}
                 (dissoc (sut/read-config) :client-id)))))

      (testing "should use the dataset backups if set in configuration"
        (with-redefs [config/read-config (constantly {:server {:media-importers 8}
                                                      :paths {:root "/custom/path/to/root"}
                                                      :datasets {:default {:paths {:backup "/custom/backups"}}}
                                                      :species-name-style :common})
                      paths/get-default-dataset-paths (constantly {:database "/path/to/database"
                                                                   :backup "/path/to/backups/default"})
                      paths/get-default-application-paths (constantly {:root "/path/to/root"
                                                                       :backup "/path/to/backups"
                                                                       :log "/path/to/logs"})]
          (is (= {:paths {:log "/path/to/logs"
                          :root "/custom/path/to/root"
                          :backup "/path/to/backups"}
                  :server {:http-port 5341
                           :media-importers 8
                           :jvm-extra-args ""}
                  :detector {:enabled false
                             :api-url "https://api.prod.camelotproject.org"
                             :confidence-threshold 0.9}
                  :datasets {:default {:name "default"
                                       :paths {:database "/path/to/database"
                                               :backup "/custom/backups"}}}
                  :language :en
                  :species-name-style :common
                  :send-usage-data false
                  :dev-mode false
                  :open-browser-on-startup true
                  :java-command "java"}
                 (dissoc (sut/read-config) :client-id))))))))

(deftest test-write-config!
  (testing "write-config!"
    (let [expected-config {:some "config"
                           :datasets {}
                           :paths {:root "/my/special/root"}}
          config {:some "config"
                  :paths {:root "/my/special/root"}}]
      (with-redefs [paths/get-config-file (constantly "config.json")
                    io/exists? (constantly true)
                    paths/get-default-application-paths (constantly {:root "/path/to/root"})
                    paths/get-default-dataset-paths (constantly {:root "/path/to/root"})
                    config/write-config! (fn [cf config] config)
                    sut/validate-config (constantly #{})]
        (testing "should elide values which match the default"
          (let [config {:some "config"
                        :paths {:root "/path/to/root"}}]
            (is (= {:datasets {} :some "config"} (sut/write-config! config)))))

        (testing "2-arg form"
          (testing "should throw if the file already exists and :overwrite? is false"
            (is (thrown? #?(:clj RuntimeException :cljs js/Error)
                         (sut/write-config! config {:overwrite? false}))))

          (testing "should save successfully if they file does not already exist"
            (with-redefs [io/exists? (constantly false)]
              (is (= expected-config (sut/write-config! config {:overwrite? false})))))

          (testing "should save successfully if file exists and :overwrite? is true"
            (is (= expected-config (sut/write-config! config {:overwrite? true}))))

          (testing "should save successfully if file exists and :overwrite? is omitted"
            (is (= expected-config (sut/write-config! config {})))))

        (testing "1-arg form"
          (testing "should throw if the file already exists and configuration is omitted"
            (is (= expected-config (sut/write-config! config))))))

      (testing "validation"
        (testing "should not save invalid configurations"
          (is (thrown-with-msg? #?(:clj RuntimeException :cljs js/Error)
                                #"The given configuration is not valid"
                                (sut/write-config! config {}))))))))

(deftest test-reset-config!
  []
  (testing "reset-config!"
    (testing "should write the default configuration"
      (let [calls (atom [])]
        (with-redefs [sut/write-config! #(swap! calls conj %)]
          (let [expected {:server {:http-port 5341
                                   :media-importers 4
                                   :jvm-extra-args ""}
                          :detector {:enabled false
                                     :api-url "https://api.prod.camelotproject.org"
                                     :confidence-threshold 0.9}
                          :language :en
                          :species-name-style :scientific
                          :send-usage-data false
                          :dev-mode false
                          :open-browser-on-startup true
                          :java-command "java"}
                expected-paths #{:application
                                 :config
                                 :backup
                                 :root
                                 :log}
                expected-dataset-paths #{:backup
                                         :filestore-base
                                         :database
                                         :media}]
            (is (= expected (dissoc (sut/reset-config!) :paths :client-id :datasets)))
            (is (= expected (dissoc (first @calls) :paths :client-id :datasets)))
            (is (= expected-paths (set (keys (:paths (first @calls))))))
            (is (= expected-dataset-paths (set (keys (get-in (first @calls) [:datasets :default :paths])))))
            (is (= 1 (count @calls)))))))))
