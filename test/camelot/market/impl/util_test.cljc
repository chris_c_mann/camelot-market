(ns camelot.market.impl.util-test
  (:require [camelot.market.impl.util :as sut]
            [camelot.market.impl.io :as io]
            #?(:clj [clojure.test :refer [testing is deftest]]
               :cljs [cljs.test :refer [testing is deftest]])))

(deftest test-initialise-dir
  (testing "initialise-dir"
    (testing "should return false if creatable but mkdir fails to create the directory"
      (with-redefs [io/exists? (constantly false)
                    io/mkdirs (constantly false)]
        (is (not (sut/initialise-dir "some-file")))))


    (testing "should return true if the directory was freshly created"
      (with-redefs [io/exists? (constantly false)
                    io/mkdirs (constantly true)]
        (is (sut/initialise-dir "some-file"))))))

(deftest test-assert-dir
  (testing "assert-dir"
    (with-redefs [io/exists? (constantly true)
                  io/mkdirs (constantly true)
                  io/directory? (constantly true)
                  io/readable? (constantly true)
                  io/writable? (constantly true)
                  io/parent (constantly "some-other-file")]
      (testing "should not create the directory should it not exist"
        (with-redefs [io/exists? (constantly false)]
          (is (thrown-with-msg? #?(:clj RuntimeException :cljs js/Error)
                                #"Directory does not exist"
                                (sut/assert-dir "some-file")))))

      (testing "should throw if not creatable and does not already exist"
        (with-redefs [io/exists? (constantly false)]
          (is (thrown-with-msg? #?(:clj RuntimeException :cljs js/Error)
                                #"Directory does not exist"
                                (sut/assert-dir "some-file")))))

      (testing "should throw if the file is not a directory"
        (with-redefs [io/directory? (constantly false)]
          (is (thrown-with-msg? #?(:clj RuntimeException :cljs js/Error)
                                #"File is not a directory"
                                (sut/assert-dir "some-file")))))

      (testing "should throw if the directory is not readable"
        (with-redefs [io/readable? (constantly false)]
          (is (thrown-with-msg? #?(:clj RuntimeException :cljs js/Error)
                                #"Directory is not readable"
                                (sut/assert-dir "some-file")))))

      (testing "should throw if the directory is not writable"
        (with-redefs [io/writable? (constantly false)]
          (is (thrown-with-msg? #?(:clj RuntimeException :cljs js/Error)
                                #"Directory is not writable"
                                (sut/assert-dir "some-file")))))

      (testing "should return the given file if all assertions pass"
        (is (= "some-file" (sut/assert-dir "some-file")))))))

(deftest test-assert-parent-dir
  (testing "assert-parent-dir"
    (testing "should return the original directory"
      (with-redefs [io/exists? (constantly true)
                    io/mkdirs (constantly true)
                    io/directory? (constantly true)
                    io/readable? (constantly true)
                    io/writable? (constantly true)
                    io/parent (constantly "parent-file")]
        (is (= "some-file" (sut/assert-parent-dir "some-file")))))))

(deftest test-diff-hashmaps
  (testing "diff-hashmaps"
    (testing "should return empty map if all KV pairs identical"
      (is (= {} (sut/diff-hashmaps {:key 42} {:key 42}))))

    (testing "should return a if b is nil"
      (is (= {:key 42} (sut/diff-hashmaps {:key 42} nil))))

    (testing "should return empty map if a is nil"
      (is (= {} (sut/diff-hashmaps nil {:key 42}))))

    (testing "should return the difference of a and b"
      (is (= {:k2 "this"} (sut/diff-hashmaps {:k1 "same" :k2 "this"} {:k1 "same" :k2 "that"}))))

    (testing "should return differences in a nested map"
      (is (= {:k {:k2 "this"}}
             (sut/diff-hashmaps {:k {:k1 "same" :k2 "this"}}
                                {:k {:k1 "same" :k2 "that"}}))))

    (testing "should elide identical values in the upper level"
      (is (= {:k {:k2 "this"}}
             (sut/diff-hashmaps {:k {:k1 "same" :k2 "this"}}
                                {:k {:k1 "same" :k2 "that"}}))))))
