(ns camelot.market.impl.environ-test
  (:require [camelot.market.impl.environ :as sut]
            #?(:cljs [cljs.test :refer [testing is deftest]])))

#?(:cljs
   (deftest test-env
     (testing "env"
       (with-redefs [sut/process #js {"env" #js {"CAMELOT_DATADIR" "/path/to/datadir"}}]
         (is (= (sut/env :camelot-datadir) "/path/to/datadir"))))))
