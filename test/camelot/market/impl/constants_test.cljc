(ns camelot.market.impl.constants-test
  (:require [camelot.market.impl.constants :as sut]
            [camelot.market.spec :as market-s]
            [clojure.spec.alpha :as s]
            #?(:clj [clojure.test :refer [testing deftest is]]
               :cljs [cljs.test :refer [testing is deftest]])))

(deftest test-default-config
  (testing "default-config"
    (testing "should be a valid environment-agnostic configuration"
      (is (s/valid? ::market-s/environment-agnostic-config
                    sut/default-config)))))
