(ns camelot.market.spec
  (:require [clojure.spec.alpha :as s]))

(def min-max-heap-size 1024)
(def max-media-importers 20)
(def max-http-port 65535)

(s/def ::client-id string?)

(s/def :path/media string?)
(s/def :path/filestore-base string?)
(s/def :path/backup string?)
(s/def :path/database string?)
(s/def :path/log string?)
(s/def :path/config string?)
(s/def :path/root string?)
(s/def :path/application string?)

(s/def ::dev-mode boolean?)
(s/def ::features (s/map-of keyword? boolean?))
(s/def ::http-port (s/and pos-int? #(<= % max-http-port)))
(s/def ::max-heap-size (s/nilable (s/and pos-int? #(>= % min-max-heap-size))))
(s/def ::java-command string?)
(s/def ::jvm-extra-args string?)
(s/def ::language #{:en})
(s/def ::media-importers (s/and pos-int? #(<= % max-media-importers)))
(s/def ::open-browser-on-startup boolean?)

(s/def :detector/enabled (s/nilable boolean?))
(s/def :detector/username (s/nilable string?))
(s/def :detector/password (s/nilable string?))
(s/def :detector/confidence-threshold (s/and float? #(and (>= % 0.0) (<= % 1.0))))
(s/def :detector/api-url string?)

(s/def ::paths
  (s/keys :req-un [:path/config
                   :path/root
                   :path/backup
                   :path/application
                   :path/log]
          ;; pre-1.7.x only
          :opt-un [:path/media
                   :path/filestore-base
                   :path/database]))

(s/def :dataset/name string?)
(s/def :dataset/paths
  (s/keys :req-un [:path/media
                   :path/filestore-base
                   :path/database]
          ;; Optional to allow blanking during write
          :opt-un [:path/backup]))

(s/def ::send-usage-data boolean?)
(s/def ::species-name-style #{:scientific :common-name})
(s/def ::timezone string?)

(s/def ::server
  (s/keys :req-un [::http-port
                   ::media-importers]
          :opt-un [::max-heap-size
                   ::jvm-extra-args]))

(s/def ::environment-specific-config
  (s/keys :req-un [::paths]))

(s/def :dataset/config
  (s/keys :req-un [:dataset/name
                   :dataset/paths]))

(s/def :dataset/datasets
  (s/map-of keyword? :dataset/config))

(s/def :dataset/keys
  ;; ::datasets optional prior to ~1.7.x.
  (s/keys :opt-un [:dataset/datasets]))

(s/def ::detector
  (s/keys :req-un [:detector/enabled
                   :detector/api-url
                   :detector/confidence-threshold]
          :opt-un [:detector/username
                   :detector/password]))

(s/def ::environment-agnostic-config
  (s/keys :req-un [::server
                   ::detector
                   ::java-command
                   ::language
                   ::open-browser-on-startup
                   ::send-usage-data
                   ::species-name-style]
          :opt-un [::features
                   ::dev-mode
                   ::timezone]))

(s/def ::dynamic-config
  ;; client-id optional to support old configs prior to 1.6.0
  (s/keys :opt-un [::client-id]))

(s/def ::config (s/merge ::environment-agnostic-config
                         ::environment-specific-config
                         :dataset/keys
                         ::dynamic-config))
