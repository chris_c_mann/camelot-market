(ns camelot.market.impl.constants
  "Camelot configuration constants and defaults.")

(def default-dataset-id :default)

(def backup-directory-name
  "Backups")

(def media-directory-name
  "Media")

(def filestore-directory-name
  "FileStore")

(def log-directory-name
  "Logs")

(def config-filename
  "config.json")

(def legacy-config-filename
  "config.clj")

(def detector-api-url
  "https://api.prod.camelotproject.org")

(def default-config
  "Return the default configuration."
  {:server {:http-port 5341
            :media-importers 4
            :jvm-extra-args ""}
   :detector {:enabled false
              :api-url detector-api-url
              :confidence-threshold 0.9}
   :language :en
   :species-name-style :scientific
   :send-usage-data false
   :dev-mode false
   :open-browser-on-startup true
   :java-command "java"})

(def paths-to-keyword-values
  "Paths to configuration values which should be transformed to keywords."
  [[:language]
   [:species-name-style]
   [:launcher :ui-mode]])
