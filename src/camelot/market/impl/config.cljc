(ns camelot.market.impl.config
  "Manage Camelot configuration files."
  (:require [camelot.market.impl.paths :as paths]
            [camelot.market.impl.io :as io]
            [camelot.market.impl.constants :as constants]
            [camelot.market.impl.util :as util]
            #?(:clj  [clojure.data.json :as json])
            #?(:clj  [clojure.edn :as edn]
               :cljs [cljs.reader :as reader]))
  #?(:clj (:import (java.util UUID))))

(defn- as-json
  [s]
  #?(:clj  (json/read-str s)
     :cljs (js->clj (.parse js/JSON s))))

(defn- as-edn
  [s]
  #?(:clj  (edn/read-string s)
     :cljs (reader/read-string s)))

(defn- to-json
  [s]
  #?(:clj  (json/write-str s)
     :cljs (.stringify js/JSON (clj->js s))))

(defn- keywordify-keys
  [c]
  (cond
    (or (seq? c) (vector? c))
    (mapv keywordify-keys c)

    (map? c)
    (reduce-kv (fn [acc k v]
                 (assoc acc (keyword k) (if (coll? v)
                                          (keywordify-keys v)
                                          v)))
               {} c)

    :else
    c))

(defn- keywordify-values
  [c]
  (reduce
   (fn [acc path]
     (if (get-in acc path)
       (update-in acc path keyword)
       acc))
   c constants/paths-to-keyword-values))

(def server-keys #{:media-importers :http-port :jvm-extra-args :max-heap-size})
(defn upgrade-shape
  [legacy-config]
  (reduce-kv
   (fn [acc k v]
     (assoc-in acc (if (contains? server-keys k)
                     [:server k]
                     [k])
               v))
   {}
   legacy-config))

(defn- gen-client-id
  []
  #?(:clj (.toString (UUID/randomUUID))
     :cljs (str (random-uuid))))

(defn read-config
  "Read the configuration from file."
  [cf]
  (if (io/exists? cf)
    (->> cf
         io/slurp
         as-json
         keywordify-keys
         keywordify-values)
    (let [cf (paths/get-legacy-config-file)]
      (when (io/exists? cf)
        (-> cf io/slurp as-edn upgrade-shape)))))

(defn- assoc-default-dataset-paths
  [datasets c]
  (letfn [(reducer [acc dataset]
            (update-in acc [:datasets dataset :paths]
                       #(merge (paths/get-default-dataset-paths (:paths acc) dataset) %)))]
    (reduce reducer c (set datasets))))

(defn- assoc-default-dataset-names
  [config]
  (letfn [(reducer [acc dataset]
            (assoc-in acc [:datasets dataset :name] (name dataset)))]
    (reduce reducer {} (set (keys (:datasets config))))))

(defn- assoc-default-application-paths
  [s]
  (assoc s :paths (paths/get-default-application-paths)))

(def ^:private assoc-default-paths
  (comp #(assoc-default-dataset-paths (keys (:datasets %)) %) assoc-default-application-paths))

(defn hydrate
  "Hydrate the configuration with default values."
  [config]
  (util/deep-merge constants/default-config
                   (assoc-default-paths config)
                   (assoc-default-dataset-names config)
                   config))

(defn canonicalize-nil
  [m]
  (into {} (remove (fn [[_ v]] (nil? v)) m)))

(defn upgrade
  "Upgrade the given configuration"
  [config & {:keys [client-id-generator]
             :or {client-id-generator gen-client-id}}]
  (let [dataset-path-keys [:media :filestore-base :backup :database]
        dataset-paths (select-keys (:paths config) dataset-path-keys)]
    (cond-> config
      ;; client-id
      (nil? (:client-id config))
      (assoc :client-id (client-id-generator))

      ;; datasets
      (nil? config)
      (assoc :datasets {:default {}})

      (and config (nil? (:datasets config)))
      (-> (update :paths #(apply dissoc % dataset-path-keys))
          (update-in [:datasets constants/default-dataset-id :paths]
                     #(merge (paths/get-historic-paths :v1.5.x)
                             dataset-paths
                             %)))

      :always
      (canonicalize-nil))))

(defn get-default-config
  []
  (->> {:datasets {:default {:name "Default"}}}
       upgrade
       hydrate))

(defn- get-default-config-for-datasets
  [config]
  (assoc-default-dataset-paths (keys (:datasets config))
                               (get-default-config)))

(defn- minimum-dataset-config
  [datasets]
  (->> datasets
       keys
       (map (fn [k] (vector k {})))
       (into {})))

(defn dehydrate
  [config]
  (merge {:datasets (minimum-dataset-config (:datasets config))}
         (util/diff-hashmaps config (get-default-config-for-datasets config))))

(defn write-config!
  [cf config]
  (let [cftmp (io/create-temp-file "camelot-config" ".json")]
    (io/spit cftmp (to-json config))
    (let [result (io/rename cftmp cf)]
      (when-not result
        (throw (ex-info "Failed to rename temporary configuration file"
                        {:from cftmp
                         :to cf})))
      (when (io/exists? (paths/get-legacy-config-file))
        (io/delete (paths/get-legacy-config-file)))))
  config)
