(ns camelot.market.migration
  "Migration utilities from previous configuration versions."
  (:require
   [camelot.market.impl.paths :as paths]))

(def get-historic-paths
  "Historic paths. Takes a version key such as `:v1.6.x`."
  paths/get-historic-paths)
