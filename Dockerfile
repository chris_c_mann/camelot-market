# Docker image for CI

FROM clojure:openjdk-11-lein
MAINTAINER Chris Mann <chris@bitpattern.com.au>

RUN apt-get update -y
RUN apt-get install nodejs curl -y
RUN mkdir -p ~/bin
RUN ln -s /usr/bin/nodejs ~/bin/node

RUN curl -O https://download.clojure.org/install/linux-install-1.10.0.411.sh
RUN chmod +x linux-install-1.10.0.411.sh
RUN ./linux-install-1.10.0.411.sh

WORKDIR /tmp
COPY project.clj /tmp/
COPY deps.edn /tmp/
COPY src /tmp/src/
COPY script /tmp/script
COPY test /tmp/test

ENV PATH="/root/bin:$PATH"

RUN LEIN_ROOT='yes' lein check
RUN script/test

RUN rm -rf /tmp/*
