# Changelog
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## [Unreleased]
### Added
- Add cljs support
### Fixed
- No longer require configuration file to exist for read-config
### Changed
- Allow creation of the config and data base directories

[Unreleased]: https://gitlab.com/camelot-project/camelot-market/compare/v0.0.1...HEAD
